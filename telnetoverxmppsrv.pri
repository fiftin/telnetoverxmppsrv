HEADERS += \
    base/telnetoverxmppbase.h \
    base/connectioninfo.h \
    base/connectionbase.h \
    base/message2.h \
    base/filemessage.h \
    telnetoverxmppsrv.h \
    itelnetoverxmppsrv.h \
    session.h \

SOURCES +=  \
    base/telnetoverxmppbase.cpp \
    base/connectionbase.cpp \
    base/message2.cpp\
    base/filemessage.cpp \
    telnetoverxmppsrv.cpp \
    session.cpp \
